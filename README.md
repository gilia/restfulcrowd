# **crowd Restful API for manipulating OWL 2 ontologies**

*This restful api encapsulate and enrich the main functions provided by the (owlready2)[https://bitbucket.org/jibalamy/owlready/src/default/] package. The idea is to serve some operations
for manipulating realistic ontologíes. 

The project of this API is framed into our [crowd](crowd.fi.uncoma.edu.ar) main project for managing the interaction between 
logical formalisms and visual environments.*

**crowd Restful API** has been created by the Departamento de Teoría de la Computación at Universidad Nacional del Comahue 
y el Laboratorio de Investigación y Desarrollo en Ingeniería de Software y Sistemas de Información (LISSI) at Universidad Nacional del Sur. 

It is available under the GNU LGPL licence v3

**Feel free to fork this repo and code with us.**

## **ToDo**

- User support.

- Documentation.

- Support for more operations.

## **DISCLAIMER** 
*No user support has been configured yet! This API is completely open and anyone using it could access to any ontology!*

## **http://crowd.fi.uncoma.edu.ar/restfulcrowd/ontology/**

## ** How to use **

Note: you could need installing [httpie](https://httpie.org/) to interact with this powerful API :) (anyway you could use curl too)

#### list of ontologies and classes

*http --form GET http://crowd.fi.uncoma.edu.ar/restfulcrowd/ontology/*

#### list of classes, object properties and data properties

*http --form GET http://crowd.fi.uncoma.edu.ar/restfulcrowd/classes/*

*http --form GET http://crowd.fi.uncoma.edu.ar/restfulcrowd/objectproperties/*

*http --form GET http://crowd.fi.uncoma.edu.ar/restfulcrowd/dataproperties/*

#### put a new ontology

*http --form POST http://crowd.fi.uncoma.edu.ar/restfulcrowd/ontology/ uri=[owl uri] name=[name]*

*http --form POST http://crowd.fi.uncoma.edu.ar/restfulcrowd/ontology/ uri="https://protege.stanford.edu/ontologies/pizza/pizza.owl" name="pizza"*

#### delete an ontology

*http --form DELETE http://crowd.fi.uncoma.edu.ar/restfulcrowd/ontology/id/*

The API (automatically) generates classes, object properties and data properties (their domains and ranges)


## **Local Installation**

1) install python3  (https://dzone.com/articles/install-python-370-on-ubuntu-1804debian-95)

2) install pip3

3) install 

- coreapi (1.32.0+) - Schema generation support.

- Markdown (2.1.0+) - Markdown support for the browsable API.

- django-filter (1.0.1+) - Filtering support.

6) install virtualenv and set a new one. For example: virtualenv env

7) source env/bin/activate

8) pip3 install django

10) pip3 install [djangorestframework](https://www.django-rest-framework.org/)

11) pip3 install [owlready2](https://bitbucket.org/jibalamy/owlready/src/default/)

12) python3 manage.py runserver