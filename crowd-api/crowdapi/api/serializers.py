from django.contrib.auth.models import User, Group
from crowdapi.api.models import Ontology, ObjectProperties, DataProperties, Classes, ObjectPropertiesDomain, ObjectPropertiesRange, DataPropertiesDomain, DataPropertiesRange
from rest_framework import serializers
import owlready2


class OntologySerializer(serializers.HyperlinkedModelSerializer):

    classes = serializers.HyperlinkedRelatedField(
            many=True, view_name='classes-detail', read_only=True)
    objectproperties = serializers.HyperlinkedRelatedField(
            many=True, view_name='objectproperties-detail', read_only=True)
    dataproperties = serializers.HyperlinkedRelatedField(
            many=True, view_name='dataproperties-detail', read_only=True)

    id = serializers.IntegerField(label='ID', read_only=True)

    class Meta:
        model = Ontology
        fields = ('id', 'uri', 'name', 'classes', 'objectproperties', 'dataproperties')

    def create(self, validated_data):
        ontology = Ontology.objects.create(**validated_data)
        onto = owlready2.get_ontology(validated_data['uri'])
        onto.load()
        classes = list(onto.classes())
        objectproperties = list(onto.object_properties())
        dataproperties = list(onto.data_properties())
        prefix = onto.name

        for aclass in classes:
            Classes.objects.create(ontology=ontology, uri=validated_data['uri'], name=aclass, prefix=prefix)

        for aobjprop in objectproperties:
            op = ObjectProperties.objects.create(ontology=ontology, uri=validated_data['uri'], name=aobjprop, prefix=prefix)

            opdomain = aobjprop.get_domain()
            oprange = aobjprop.get_range()

            for aopdomain in opdomain:
                ObjectPropertiesDomain.objects.create(objectproperty=op, uri=validated_data['uri'], name=aopdomain.name, prefix=prefix)  

            for aoprange in oprange:
                ObjectPropertiesRange.objects.create(objectproperty=op, uri=validated_data['uri'], name=aoprange.name, prefix=prefix)  

        for adataprop in dataproperties:
            dp = DataProperties.objects.create(ontology=ontology, uri=validated_data['uri'], name=adataprop, prefix=prefix)

            dpdomain = adataprop.get_domain()
            dprange = adataprop.get_range()

            for adpdomain in dpdomain:
                DataPropertiesDomain.objects.create(dataproperty=dp, uri=validated_data['uri'], name=adpdomain.name, prefix=prefix)  

            for adprange in dprange:
                DataPropertiesRange.objects.create(dataproperty=dp, uri=validated_data['uri'], name=adprange.name, prefix=prefix)

        return ontology
 
class ClassesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Classes
        fields = ('ontology','uri', 'name', 'prefix')

class ObjectPropertiesSerializer(serializers.HyperlinkedModelSerializer):
   
    objectpropertiesdomain = serializers.HyperlinkedRelatedField(
            many=True, view_name='objectpropertiesdomain-detail', read_only=True)
    objectpropertiesrange = serializers.HyperlinkedRelatedField(
            many=True, view_name='objectpropertiesrange-detail', read_only=True)

    class Meta:
        model = ObjectProperties
        fields = ('ontology','uri', 'name', 'prefix', 'objectpropertiesdomain', 'objectpropertiesrange')

class DataPropertiesSerializer(serializers.HyperlinkedModelSerializer):

    datapropertiesdomain = serializers.HyperlinkedRelatedField(
            many=True, view_name='datapropertiesdomain-detail', read_only=True)
    datapropertiesrange = serializers.HyperlinkedRelatedField(
            many=True, view_name='datapropertiesrange-detail', read_only=True)

    class Meta:
        model = DataProperties
        fields = ('ontology','uri', 'name', 'prefix', 'datapropertiesdomain', 'datapropertiesrange')

class ObjectPropertiesDomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ObjectPropertiesDomain
        fields = ('objectproperty','uri', 'name', 'prefix')

class ObjectPropertiesRangeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ObjectPropertiesRange
        fields = ('objectproperty','uri', 'name', 'prefix')

class DataPropertiesDomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataPropertiesDomain
        fields = ('dataproperty','uri', 'name', 'prefix')

class DataPropertiesRangeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataPropertiesRange
        fields = ('dataproperty','uri', 'name', 'prefix')
