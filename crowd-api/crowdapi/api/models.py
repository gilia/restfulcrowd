from django.db import models

# Create your models here.

class Ontology(models.Model):
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)

    class Meta:
        unique_together = ('uri', 'name')

    def __str__(self):
        return '%s: %s' % (self.uri, self.name)

class Classes(models.Model):
    ontology = models.ForeignKey(Ontology, related_name='classes', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class ObjectProperties(models.Model):
    ontology = models.ForeignKey(Ontology, related_name='objectproperties', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class DataProperties(models.Model):
    ontology = models.ForeignKey(Ontology, related_name='dataproperties', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class ObjectPropertiesDomain(models.Model):
    objectproperty = models.ForeignKey(ObjectProperties, related_name='objectpropertiesdomain', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class ObjectPropertiesRange(models.Model):
    objectproperty = models.ForeignKey(ObjectProperties, related_name='objectpropertiesrange', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class DataPropertiesDomain(models.Model):
    dataproperty = models.ForeignKey(DataProperties, related_name='datapropertiesdomain', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)

class DataPropertiesRange(models.Model):
    dataproperty = models.ForeignKey(DataProperties, related_name='datapropertiesrange', on_delete=models.CASCADE)
    uri = models.CharField('URI', max_length=1000)
    name = models.CharField('Name', max_length=120)
    prefix = models.CharField('Prefix', max_length=120, null = True)
