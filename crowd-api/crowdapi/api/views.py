from django.shortcuts import render
from django.core import serializers
from django.contrib.auth.models import User, Group
from crowdapi.api.models import Ontology, Classes, DataProperties, ObjectProperties, ObjectPropertiesDomain, ObjectPropertiesRange, DataPropertiesDomain, DataPropertiesRange
from rest_framework import viewsets, generics
from crowdapi.api.serializers import OntologySerializer, ClassesSerializer, ObjectPropertiesSerializer, DataPropertiesSerializer, ObjectPropertiesDomainSerializer, ObjectPropertiesRangeSerializer, DataPropertiesDomainSerializer, DataPropertiesRangeSerializer

# Create your views here.

class OntologyList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies list() and create().
    """

    queryset = Ontology.objects.all()
    serializer_class = OntologySerializer


class OntologyDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies retrieve(), update() and destroy()
    """

    queryset = Ontology.objects.all()
    serializer_class = OntologySerializer


class ClassesList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies classes list() and create().
    """

    queryset = Classes.objects.all()
    serializer_class = ClassesSerializer


class ClassesDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies classes retrieve(), update() and destroy()
    """

    queryset = Classes.objects.all()
    serializer_class = ClassesSerializer

class ObjectPropertiesList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies object properties list() and create().
    """

    queryset = ObjectProperties.objects.all()
    serializer_class = ObjectPropertiesSerializer

class ObjectPropertiesDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies object properties retrieve(), update() and destroy()
    """

    queryset = ObjectProperties.objects.all()
    serializer_class = ObjectPropertiesSerializer

class ObjectPropertiesDomainList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies object properties domains list() and create().
    """

    queryset = ObjectPropertiesDomain.objects.all()
    serializer_class = ObjectPropertiesDomainSerializer

class ObjectPropertiesDomainDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies object properties domains retrieve(), update() and destroy()
    """

    queryset = ObjectPropertiesDomain.objects.all()
    serializer_class = ObjectPropertiesDomainSerializer

class ObjectPropertiesRangeList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies object properties ranges list() and create().
    """

    queryset = ObjectPropertiesRange.objects.all()
    serializer_class = ObjectPropertiesRangeSerializer

class ObjectPropertiesRangeDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies object properties ranges retrieve(), update() and destroy()
    """

    queryset = ObjectPropertiesRange.objects.all()
    serializer_class = ObjectPropertiesRangeSerializer

class DataPropertiesList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies data properties list() and create().
    """

    queryset = DataProperties.objects.all()
    serializer_class = DataPropertiesSerializer

class DataPropertiesDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies data properties retrieve(), update() and destroy()
    """

    queryset = DataProperties.objects.all()
    serializer_class = DataPropertiesSerializer

class DataPropertiesDomainList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies data properties domains list() and create().
    """

    queryset = DataPropertiesDomain.objects.all()
    serializer_class = DataPropertiesDomainSerializer

class DataPropertiesDomainDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies data properties domains retrieve(), update() and destroy()
    """

    queryset = DataPropertiesDomain.objects.all()
    serializer_class = DataPropertiesDomainSerializer

class DataPropertiesRangeList(generics.ListCreateAPIView):
    """
    API endpoint that allows ontologies data properties range list() and create().
    """

    queryset = DataPropertiesRange.objects.all()
    serializer_class = DataPropertiesRangeSerializer

class DataPropertiesRangeDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    API endpoint that allows ontologies data properties range retrieve(), update() and destroy()
    """

    queryset = DataPropertiesRange.objects.all()
    serializer_class = DataPropertiesRangeSerializer
