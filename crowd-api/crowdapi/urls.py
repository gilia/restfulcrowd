"""crowdapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
"""from django.conf.urls import include
from django.conf.urls import url
from rest_framework.documentation import include_docs_urls"""
"""from rest_framework import routers"""
from crowdapi.api import views


"""router = routers.DefaultRouter()
router.register(r'classes', views.ClassesViewSet)
router.register(r'objectproperties', views.ObjectPropertiesViewSet)
router.register(r'objectpropertydomain', views.ObjectPropertiesDomainViewSet)
router.register(r'objectpropertyrange', views.ObjectPropertiesRangeViewSet)
router.register(r'dataproperties', views.DataPropertiesViewSet)
router.register(r'datapropertydomain', views.DataPropertiesDomainViewSet)
router.register(r'datapropertyrange', views.DataPropertiesRangeViewSet)"""
"""path('admin/', admin.site.urls),
 
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^docs/', include_docs_urls(title='crowd API for visual ontology engineering', authentication_classes=[], permission_classes=[])),"""

urlpatterns = [
    path('ontology/', views.OntologyList.as_view()),
    path('ontology/<int:pk>/', views.OntologyDetail.as_view(), name='ontology-detail'),
    path('classes/', views.ClassesList.as_view()),
    path('classes/<int:pk>/', views.ClassesDetail.as_view(), name='classes-detail'),
    path('objectproperties/', views.ObjectPropertiesList.as_view()),
    path('objectproperties/<int:pk>/', views.ObjectPropertiesDetail.as_view(), name='objectproperties-detail'),
    path('objectpropertiesdomain/', views.ObjectPropertiesDomainList.as_view()),
    path('objectpropertiesdomain/<int:pk>/', views.ObjectPropertiesDomainDetail.as_view(), name='objectpropertiesdomain-detail'),
    path('objectpropertiesrange/', views.ObjectPropertiesRangeList.as_view()),
    path('objectpropertiesrange/<int:pk>/', views.ObjectPropertiesRangeDetail.as_view(), name='objectpropertiesrange-detail'),
    path('dataproperties/', views.DataPropertiesList.as_view()),
    path('dataproperties/<int:pk>/', views.DataPropertiesDetail.as_view(), name='dataproperties-detail'),
    path('datapropertiesdomain/', views.DataPropertiesDomainList.as_view()),
    path('datapropertiesdomain/<int:pk>/', views.DataPropertiesDomainDetail.as_view(), name='datapropertiesdomain-detail'),
    path('datapropertiesrange/', views.DataPropertiesRangeList.as_view()),
    path('datapropertiesrange/<int:pk>/', views.DataPropertiesRangeDetail.as_view(), name='datapropertiesdomain-detail'),

    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]
